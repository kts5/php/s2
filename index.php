<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S2: Arrays and Loops Activity</title>
</head>
<body>

	<h1>Divisibles of Five</h1>

	<?php numLoop(); ?>

	<br><br>

	<h1>Array Manipulation</h1>
	<?php array_push($students, 'John Smith') ?>

	<p>
		<?php print_r($students) ?>
	</p>

	<p><?php echo count($students) ?></p>

	<p><?php array_push($students, 'Jane Smith') ?></p>

	<p><?php print_r($students) ?></p>

	<p><?php echo count($students) ?></p>

	<p>
		<?php array_shift($students) ?>
	</p>

	<p>
		<?php print_r($students) ?>
	</p>

	<p><?php echo count($students) ?></p>

</body>
</html>